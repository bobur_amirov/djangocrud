from django.urls import path
from .views import CreateList, ListView, UpdateView, DeleteView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', ListView, name='list_view'),
    path('create', CreateList, name='create'),
    path('update/<int:id>', UpdateView, name='update'),
    path('delete/<int:id>', DeleteView, name='delete'),
]



if settings.DEBUG: 
        urlpatterns += static(settings.MEDIA_URL, 
                              document_root=settings.MEDIA_ROOT) 