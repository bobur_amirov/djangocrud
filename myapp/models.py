from django.db import models

# Create your models here.

class News(models.Model):
    title = models.CharField(max_length=100)
    images = models.ImageField(upload_to='images/', blank=True, null=True)
    description = models.TextField()
    add_date = models.DateField(auto_now_add=True)
    author = models.CharField(max_length=100)

    def __str__(self):
        return self.title