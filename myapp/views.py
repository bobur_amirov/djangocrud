from django.shortcuts import render, redirect, get_object_or_404
from .models import News
from .forms import NewsForm

# Create your views here.


def CreateList(request):
    form = NewsForm()
    if request.method == "POST":
        form = NewsForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
        
    return render(request, "create.html", {'form': form})

def ListView(request):  

    news = News.objects.all()

    return render(request, "list_view.html", {'news': news})

def UpdateView(request, id): 
  
    news = News.objects.get(id=id)
    form = NewsForm(instance=news) 

    if request.method == "POST":
        form = NewsForm(request.POST,request.FILES, instance=news)  
        if form.is_valid(): 
            form.save() 
            return redirect('/')
 
  
    return render(request, "update.html", {'form': form})

def DeleteView(request, id): 
  
    news = News.objects.get(id=id)
    form = NewsForm(instance=news) 

    if request.method == "POST":
        news.delete()
        return redirect('/')
 
  
    return render(request, "delete.html", {'form': form})